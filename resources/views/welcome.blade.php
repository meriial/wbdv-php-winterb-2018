@extends('layout')

@section('other')
    <h2>hahaha</h2>
@endsection

@section('content')
    <header class="bg1">
        <div class="">
            <div class="container">
                div 1
            </div>
        </div>
        <div class="header-image">
            <img src="https://pbs.twimg.com/profile_banners/25073877/1519516126/1500x500" alt="">
        </div>
        <div class="padding-bottom-5 padding-top-5">
            <div class="container flex-horizontal flex">
                <?php foreach ($metrics as $metric): ?>
                    @include('user_metrics')
                <?php endforeach; ?>
            </div>
        </div>
    </header>

    <h1>Twitter</h1>
    <h2>Users</h2>
    <ul>
        <?php foreach($users as $user): ?>
            <li>
                <?php echo $user->name ?>
            </li>
        <?php endforeach; ?>
    </ul>

    <h2>Tweets</h2>
    <ul>
        <?php foreach($tweets as $tweet): ?>
            <li>
                <?php echo $tweet->content ?>
            </li>
        <?php endforeach; ?>
    </ul>
@endsection
