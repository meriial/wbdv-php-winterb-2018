<div class="padding-right-20">
    <div class="flex flex-vertical text-center">
        <span class="block ts-8 bold">
            <?php echo $metric['metric'] ?>
        </span>
        <span class="block ts-12 lh-1">
            <?php echo $metric['value'] ?>
        </span>
    </div>
</div>
