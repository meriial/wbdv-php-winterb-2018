<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
use Faker\Factory;

class User {}
class Tweet {}

Route::get('/about', function() {
    return view('about');
});

Route::get('/', function () {
    $faker = Factory::create();

    $user1 = new User();
    $user1->name = $faker->name;

    $tweet1 = new Tweet();
    $tweet1->content = $faker->text(280);

    $metrics = [
        ['metric' => 'Tweets', 'value' => '37K'],
        ['metric' => 'Following', 'value' => '37K'],
        ['metric' => 'Followers', 'value' => '37K'],
        ['metric' => 'Likes', 'value' => '37K'],
        ['metric' => 'Moments', 'value' => '37K'],
    ];

    $data = [
        'users' => [$user1],
        'tweets' => [$tweet1],
        'metrics' => $metrics
    ];

    return view('welcome', $data);
});







//
